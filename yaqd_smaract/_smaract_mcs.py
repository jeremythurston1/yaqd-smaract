__all__ = ["SmaractMcs"]

import logging
import pathlib
import asyncio
from typing import Dict, Any, List
import sys

from .MCSControlPythonWrapper.MCSControl_PythonWrapper import *
from yaqd_core import IsHomeable, HasLimits, HasPosition, IsDaemon


class SmaractMcs(IsHomeable, HasLimits, HasPosition, IsDaemon):
    _kind = "smaract-mcs"

    def __init__(self, name: str, config: Dict[str, Any], config_filepath: pathlib.Path):
        super().__init__(name, config, config_filepath)

        # Channels
        X1 = 0
        Y1 = 1
        Z1 = 2
        X2 = 3
        Y2 = 4
        Z2 = 5
        X3 = 6
        Y3 = 7
        Z3 = 8

        # Initialize CTypes variables
        self._mcsHandle = ct.c_ulong()
        self._numOfChannels = ct.c_ulong(0)
        self._sensorType = ct.c_ulong()
        self._channel = X1
        self._sensorEnabled = ct.c_ulong(0)
        self._ioBufferSize = ct.c_ulong(18)
        self._outBuffer = ct.create_string_buffer(17)
        self._status = ct.c_ulong()
        self._packet = SA_packet() 

        # Unused variables
        # channelA = 0
        # channelB = 1
        # stop = 0
        # chanAStopped = 0
        # chanBStopped = 0

        # Find devices 
        logging.info('Looking for SmarAct devices...')
        self.ExitIfError(SA_FindSystems('', self._outBuffer, self._ioBufferSize)) #will report a 'MCS error: query-buffer size' if outBuffer, ioBufferSize is too small
        logging.info(f"MCS address: {self._outBuffer[:18].decode('utf-8')}") #connect to first system of list
        self.ExitIfError(SA_OpenSystem(self._mcsHandle, self._outBuffer, bytes('async,reset',"utf-8")))
        logging.info('Enabling Sensor')
        self.ExitIfError(SA_InitSystems(SA_INIT_STATE_ASYNC))
        logging.info('Sensor Initialized')
        self.ExitIfError(SA_SetSensorEnabled_A(self._mcsHandle, SA_SENSOR_POWERSAVE))
        logging.info(f"MCS Handle: {self._mcsHandle.value}")
        self.ExitIfError(SA_GetNumberOfChannels(self._mcsHandle, self._numOfChannels))
        logging.info(f"Number of Channels: {self._numOfChannels.value}")
        self.ExitIfError(SA_SetBufferedOutput_A(self._mcsHandle, SA_BUFFERED_OUTPUT))
        logging.info(f"Buffered Output: {SA_BUFFERED_OUTPUT}")

        self.ExitIfError(SA_GetSensorEnabled_A(self._mcsHandle))
        self.ExitIfError(SA_FlushOutput_A(self._mcsHandle))
        self.ExitIfError(SA_ReceiveNextPacket_A(self._mcsHandle, 1000, self._packet))

        if (self._packet.data1 == SA_SENSOR_DISABLED): 
            print("Sensors are disabled")
        elif (self._packet.data1 == SA_SENSOR_ENABLED): 
            print(f"Sensors are enabled") 
        elif (self._packet.data1 == SA_SENSOR_POWERSAVE): 
            print(f"Sensors are in power-save mode") 
        else:
            print(f"Error: unknown sensor power status: {self._packet.data1}")

        for channel in range(int(self._numOfChannels.value)):
            self.ExitIfError(SA_GetSensorType_A(self._mcsHandle, channel))
            self.ExitIfError(SA_FlushOutput_A(self._mcsHandle))
            self.ExitIfError(SA_ReceiveNextPacket_A(self._mcsHandle, 1000, self._packet))

            if (self._packet.data1 == SA_S_SENSOR_TYPE) or \
                (self._packet.data1 == SA_M_SENSOR_TYPE) or \
                (self._packet.data1 == SA_SC_SENSOR_TYPE) or \
                (self._packet.data1 == SA_SP_SENSOR_TYPE):
                print(f"Channel {channel}: Linear sensor present")
            elif (self._packet.data1 == SA_SR_SENSOR_TYPE):
                print(f"Channel {channel}: Rotational sensor present")
            else:
                print(f"Channel {channel}: No linear sensor present")


    def ExitIfError(self, status: Any) -> None:
        error_msg = ct.c_char_p()
        if (status != SA_OK):
            SA_GetStatusInfo(status, error_msg)
            print(f"MCS error: {error_msg.value[:].decode('utf-8')}")
        return


    def set_channel(self, channel: int) -> None:
        self._channel = int(channel)


    def _set_position(self, position: int) -> None:
        self.ExitIfError(SA_GotoPositionAbsolute_A(self._mcsHandle, self._channel, int(position), 1000))
        self.ExitIfError(SA_SetPosition_A(self._mcsHandle, self._channel, int(position)))
        self.ExitIfError(SA_FlushOutput_A(self._mcsHandle))

        print(f"Axis {self._channel} set to {position:.1f} um.")
        

    def home(self) -> None:
        """Homes the sensor to 0"""
        self.set_position(0)


    async def update_state(self) -> None:
        """Continually monitor and update the current daemon state."""
        while True:    
            self.ExitIfError(SA_GetPosition_A(self._mcsHandle, self._channel))
            self.ExitIfError(SA_FlushOutput_A(self._mcsHandle))
            self.ExitIfError(SA_ReceiveNextPacket_A(self._mcsHandle, 1000, self._packet))

            # How is the position of each channel stored??
            self._state['position'] = self._packet.data2
    
            if self._busy:
                self.ExitIfError(SA_GetStatus_A(self._mcsHandle, self._channel))
                self.ExitIfError(SA_FlushOutput_A(self._mcsHandle))
                self.ExitIfError(SA_ReceiveNextPacket_A(self._mcsHandle, 1000, self._packet))

                self._busy = (self._packet.data1 != SA_TARGET_STATUS) and \
                             (self._packet.data1 != SA_STOPPED_STATUS)
                await asyncio.sleep(0.01)
            else:
                await self._busy_sig.wait()

