__all__ = ["SmaractMcs2"]

import logging
import pathlib
import asyncio
from typing import Dict, Any, List
import ctl as sdk
import sys

from yaqd_core import HasLimits, HasPosition, IsDaemon


class SmaractMcs2(HasLimits, HasPosition, IsDaemon):
    _kind = "smaract-mcs2"

    def __init__(self, name: str, config: Dict[str, Any], config_filepath: pathlib.Path):
        super().__init__(name, config, config_filepath)

        # Find devices 
        logging.info('Looking for SmarAct devices...')
        try:
            buffer = sdk.FindDevices()
            if len(buffer) == 0:
                logging.error("No SmarAct MCS2 device found.")
                exit(1)
            self.locators = buffer.split("\n")
            for locator in self.locators:
                logging.info(f"Available SmarAct MCS2 devices: {locator}")
        except:
            logging.error("SmarAct MCS2 failed to find a device.")
            input()
            sys.exit(1)
        
        try:
            self.d_handle = sdk.Open(self.locators[0])
            print(f'SmarAct MCS2 opened {self.locators[0]}')

        except sdk.Error as e:
            print(f'SmarAct MCS2 {e.func}: {sdk.GetResultInfo(e.code)}, error: {sdk.ErrorCode(e.code).name} (0x{e.code:04X}) in line {sys.exc_info()[-1].tb_lineno}.')

        except Exception as ex:
            print(f'Unexpected error: {ex}, {type(ex)} in line: {sys.exc_info()[-1].tb_lineno}')

        finally:
            if self.d_handle != None:
                sdk.Close(self.d_handle)
                print('SmarAct MCS2 closed.')
                exit(1)

        for channel in range(self._config['channels']):
            self._calibrate(channel)
            sdk.SetProperty_i64(self.d_handle, channel, sdk.Property.MOVE_VELOCITY, self._config['velocity'])
            sdk.SetProperty_i64(self.d_handle, channel, sdk.Property.MOVE_ACCELERATION, self._config['acceleration'])
            sdk.SetProperty_i32(self.d_handle, channel, sdk.Property.AMPLIFIER_ENABLED, sdk.TRUE)

    # def close_device(self) -> None:
    #     sdk.Close(self.d_handle)
    #     logging.info('SmarAct MCS2 connection closed.')
    #     exit(1)

    ### Moved to __init__()
    # def set_properties(self, velocity: float, acceleration: float) -> None:
    #     channel = 0
    #     sdkVelocity = self._state['velocity'] * 1000000000 # mm/s
    #     sdkAcceleration = self._state['acceleration'] * 10000000000 # mm/s^2

    #     # Set move mode to relative
    #     sdk.SetProperty_i32(self.d_handle, channel, sdk.Property.MOVE_MODE, sdk.MoveMode.CL_RELATIVE)
    #     # Set move mode to absolute
    #     # sdk.SetProperty_i32(self.d_handle, channel, sdk.Property.MOVE_MODE, sdk.MoveMode.CL_ABSOLUTE)
    #     # Set move velocity 
    #     sdk.SetProperty_i64(self.d_handle, channel, sdk.Property.MOVE_VELOCITY, 2000000000)
    #     # Set move acceleration
    #     sdk.SetProperty_i64(self.d_handle, channel, sdk.Property.MOVE_ACCELERATION, 20000000000)
    #     # Enable amplifier
    #     sdk.SetProperty_i32(self.d_handle, channel, sdk.Property.AMPLIFIER_ENABLED, sdk.TRUE)
    #     # Reads the channel type to determine additional configuration:
    #     type = sdk.GetProperty_i32(self.d_handle, channel, sdk.Property.CHANNEL_TYPE)
    #     if type == sdk.ChannelModuleType.STICK_SLIP_PIEZO_DRIVER:
    #         # Set max closed loop frequency (maxCLF) to 6 kHz. This properties sets a limit for the maximum actuator driving frequency.
    #         # The maxCLF is not persistent thus set to a default value at startup.
    #         sdk.SetProperty_i32(self.d_handle, channel, sdk.Property.MAX_CL_FREQUENCY, 6000)
    #         # The hold time specifies how long the position is actively held after reaching the target.
    #         # This property is also not persistent and set to zero by default.
    #         # A value of 0 deactivates the hold time feature, the constant sdk.HOLD_TIME_INFINITE sets the time to infinite.
    #         # (Until manually stopped by "Stop") Here we set the hold time to 1000 ms.
    #         sdk.SetProperty_i32(self.d_handle, channel, sdk.Property.HOLD_TIME, 1000)
    #     elif type == sdk.ChannelModuleType.MAGNETIC_DRIVER:
    #         # Enable the amplifier (and start the phasing sequence).
    #         sdk.SetProperty_i32(self.d_handle, channel, sdk.Property.AMPLIFIER_ENABLED, sdk.TRUE)

    def _calibrate(self, channel) -> None:
        """Moves the positioner several mm to get a more precise position calculation.
        Only call this function if a device has changed channels or if a device type has changed."""
        logging.info(f'Calibrating SmarAct MCS2 channel {channel}')
        sdk.SetProperty_i32(self.d_handle, channel, sdk.Property.CALIBRATION_OPTIONS, 0)
        sdk.Calibrate(self.d_handle, channel)
        # sdk.ChannelState.CALIBRATING can be called to determine if the channel is finished calibrating.
        # This function returns automatically.

    def _set_position(self, position: float)-> None:
        """Absolute movement to position."""
        # As of now this only works with one axis.
        sdk.Move(self.d_handle, 0, position)

    def stop_movement(self, channel) -> None:
        """Stops current movement in channel. This function decelerated the positioner until it reaches zero velocity.
        Calling this function a second time will trigger a hard stop."""
        print(f'Stopping SmarAct MCS2 channel {channel}')
        sdk.Stop(self.d_handle, channel)

    async def update_state(self) -> None:
        """Continually monitor and update the current daemon state."""
        while True:
            # self._busy = False
            if sdk.ChannelState.ACTIVELY_MOVING or sdk.ChannelState.CALIBRATING:
                self._busy = True
            else:
                self._busy = False
            
            if self._busy:
                await asyncio.sleep(0.01)
            else:
                await self._busy_sig.wait()
