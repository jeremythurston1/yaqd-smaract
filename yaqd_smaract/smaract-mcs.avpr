{
    "config": {
        "acceleration": {
            "default": 10000000000,
            "doc": "The stage's translation velocity in pm/s^2",
            "type": "float"
        },
        "enable": {
            "default": true,
            "doc": "Disable this daemon. The kind entry-point will not attempt to start this daemon.",
            "origin": "is-daemon",
            "type": "boolean"
        },
        "limits": {
            "default": [
                -Infinity,
                Infinity
            ],
            "doc": "Configuration limits are strictly optional.",
            "items": "double",
            "origin": "has-limits",
            "type": "array"
        },
        "log_level": {
            "default": "info",
            "doc": "Set daemon log-level.",
            "origin": "is-daemon",
            "type": {
                "name": "level",
                "symbols": [
                    "debug",
                    "info",
                    "notice",
                    "warning",
                    "error",
                    "critical",
                    "alert",
                    "emergency"
                ],
                "type": "enum"
            }
        },
        "log_to_file": {
            "default": false,
            "doc": "Optionally force logging to a file.",
            "origin": "is-daemon",
            "type": "boolean"
        },
        "make": {
            "default": null,
            "origin": "is-daemon",
            "type": [
                "null",
                "string"
            ]
        },
        "model": {
            "default": null,
            "origin": "is-daemon",
            "type": [
                "null",
                "string"
            ]
        },
        "out_of_limits": {
            "default": "closest",
            "doc": "Control behavior of daemon when set_position is given a value outside of limits.",
            "name": "out_of_limits",
            "origin": "has-limits",
            "symbols": [
                "closest",
                "ignore",
                "error"
            ],
            "type": "enum"
        },
        "port": {
            "doc": "TCP port for daemon to occupy.",
            "origin": "is-daemon",
            "type": "int"
        },
        "serial": {
            "default": null,
            "doc": "Serial number for the particular device represented by the daemon",
            "origin": "is-daemon",
            "type": [
                "null",
                "string"
            ]
        },
        "units": {
            "default": "pm",
            "type": "string"
        },
        "velocity": {
            "default": 1000000000,
            "doc": "The stage's translation velocity in pm/s",
            "type": "float"
        },
        "x_limits": {
            "default": [
                0.0,
                100000000000.0
            ],
            "doc": "The range for the stage to travel in the x-direction",
            "items": "float",
            "type": "array"
        },
        "y_limits": {
            "default": [
                0.0,
                100000000000.0
            ],
            "doc": "The range for the stage to travel in the y-direction",
            "items": "float",
            "type": "array"
        },
        "z_limits": {
            "default": [
                0.0,
                100000000000.0
            ],
            "doc": "The range for the stage to travel in the z-direction",
            "items": "float",
            "type": "array"
        }
    },
    "doc": "",
    "fields": {
        "destination": {
            "fields": {
                "dynamic": true,
                "getter": "get_destination",
                "kind": "normal",
                "type": "double"
            },
            "name": "field",
            "type": "record"
        },
        "limits": {
            "fields": {
                "dynamic": true,
                "getter": "get_limits",
                "kind": "normal",
                "type": {
                    "items": "double",
                    "type": "array"
                }
            },
            "name": "field",
            "type": "record"
        },
        "origin": "has-position",
        "position": {
            "fields": {
                "dynamic": true,
                "getter": "get_position",
                "kind": "hinted",
                "setter": "set_position",
                "type": "double"
            },
            "name": "field",
            "type": "record"
        }
    },
    "installation": {
        "PyPI": "https://pypi.org/project/yaqd-smaract"
    },
    "links": {
        "bugtracker": "https://gitlab.com/yaq/yaqd-smaract/-/issues",
        "source": "https://gitlab.com/yaq/yaqd-smaract"
    },
    "messages": {
        "busy": {
            "doc": "Returns true if daemon is currently busy.",
            "origin": "is-daemon",
            "request": [],
            "response": "boolean"
        },
        "get_config": {
            "doc": "Full configuration for the individual daemon as defined in the TOML file.\nThis includes defaults and shared settings not directly specified in the daemon-specific TOML table.\n",
            "origin": "is-daemon",
            "request": [],
            "response": "string"
        },
        "get_config_filepath": {
            "doc": "String representing the absolute filepath of the configuration file on the host machine.\n",
            "origin": "is-daemon",
            "request": [],
            "response": "string"
        },
        "get_destination": {
            "doc": "Get current daemon destination.",
            "origin": "has-position",
            "request": [],
            "response": "double"
        },
        "get_limits": {
            "doc": "Get daemon limits.Limits will be the <a href='https://en.wikipedia.org/wiki/Intersection_(set_theory)'>intersection</a> of config limits and driver limits (when appliciable).",
            "origin": "has-limits",
            "request": [],
            "response": {
                "items": "double",
                "type": "array"
            }
        },
        "get_position": {
            "doc": "Get current daemon position.",
            "origin": "has-position",
            "request": [],
            "response": "double"
        },
        "get_state": {
            "doc": "Get version of the running daemon",
            "origin": "is-daemon",
            "request": [],
            "response": "string"
        },
        "get_units": {
            "doc": "Get units of daemon. These units apply to the position and destination fields.",
            "origin": "has-position",
            "request": [],
            "response": [
                "null",
                "string"
            ]
        },
        "home": {
            "doc": "Sets the sensor to 0 position",
            "origin": "is-homeable"
        },
        "id": {
            "doc": "JSON object with information to identify the daemon, including name, kind, make, model, serial.\n",
            "origin": "is-daemon",
            "request": [],
            "response": {
                "type": "map",
                "values": [
                    "null",
                    "string"
                ]
            }
        },
        "in_limits": {
            "doc": "Check if a given position is within daemon limits.",
            "origin": "has-limits",
            "request": [
                {
                    "name": "position",
                    "type": "double"
                }
            ],
            "response": "boolean"
        },
        "set_channel": {
            "doc": "Changes the channel being controlled",
            "request": [
                {
                    "name": "channel",
                    "type": "int"
                }
            ]
        },
        "set_position": {
            "doc": "Give the daemon a new destination, and begin motion towards that destination.",
            "origin": "has-position",
            "request": [
                {
                    "name": "position",
                    "type": "double"
                }
            ],
            "response": "null"
        },
        "set_relative": {
            "doc": "Give the daemon a new destination relative to its current position. Daemon will immediately begin motion towards new destination. Returns new destination.",
            "origin": "has-position",
            "request": [
                {
                    "name": "distance",
                    "type": "double"
                }
            ],
            "response": "double"
        },
        "shutdown": {
            "doc": "Cleanly shutdown (or restart) daemon.",
            "origin": "is-daemon",
            "request": [
                {
                    "default": false,
                    "name": "restart",
                    "type": "boolean"
                }
            ],
            "response": "null"
        }
    },
    "protocol": "smaract-mcs",
    "requires": [],
    "state": {
        "destination": {
            "default": NaN,
            "origin": "has-position",
            "type": "double"
        },
        "hw_limits": {
            "default": [
                -Infinity,
                Infinity
            ],
            "items": "double",
            "origin": "has-limits",
            "type": "array"
        },
        "position": {
            "default": NaN,
            "origin": "has-position",
            "type": "double"
        }
    },
    "traits": [
        "is-homeable",
        "has-limits",
        "has-position",
        "is-daemon"
    ],
    "types": [
        {
            "fields": [
                {
                    "name": "shape",
                    "type": {
                        "items": "int",
                        "type": "array"
                    }
                },
                {
                    "name": "typestr",
                    "type": "string"
                },
                {
                    "name": "data",
                    "type": "bytes"
                },
                {
                    "name": "version",
                    "type": "int"
                }
            ],
            "logicalType": "ndarray",
            "name": "ndarray",
            "type": "record"
        }
    ]
}
