# yaq-smaract

[![PyPI](https://img.shields.io/pypi/v/yaq-smaract)](https://pypi.org/project/yaq-smaract)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaq-smaract)](https://anaconda.org/conda-forge/yaq-smaract)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaq-smaract/-/blob/master/CHANGELOG.md)

yaq daemon for smaract controllers

This package contains the following daemon(s):

- https://yaq.fyi/daemons/smaract-mcs2

This package requires the following SDKs:

MCS2 SDK:
- https://www.dropbox.com/home/KM%20Interfacing/SmarAct/MCS2/SDK/Python

MCS SDK: 
- https://www.dropbox.com/home/KM%20Interfacing/SmarAct/MCS
